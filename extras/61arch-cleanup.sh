#!/bin/sh

# Script is called as part of the final steps before rebooting
# Make sure this script runs AFTER hw-detect in final stages
# - Currently done by script number being > 08 and < 90

rm -f /target/etc/apt/sources.list.d/parrot_arch.list

exit
