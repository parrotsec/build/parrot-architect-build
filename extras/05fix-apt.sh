#!/bin/sh

# Script used to maintain target access to the install media
# while Parrot's official sources.list are installed during tasksel

in-target mount /dev/sr0 /media/cdrom0
in-target sed -i 's/deb cdrom/deb \[trusted=yes\] cdrom/g' /etc/apt/sources.list
in-target cp /etc/apt/sources.list /etc/apt/sources.list.d/parrot_arch.list
in-target apt-get update

exit
