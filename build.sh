#!/bin/bash
#### Parrot Architect ISO Build Script ####

PARROT_DIST='lory'
PARROT_VERSION='6.3'

DEBIAN_RELEASE='bookworm'
export DEBVERSION="$PARROT_VERSION"

DEBIAN_DIRS=(
	"/usr/share/debian-cd/data"
	"/usr/share/debian-cd/tasks"
	"/usr/share/debian-cd/tools"
	"/usr/share/debian-cd/tools/boot"
)

# Check for necessary build tools
required_commands=( convert build-simple-cdd simple-cdd reprepro xorriso )
for req_comm in ${required_commands[@]}
do
	command -v ${req_comm[i]} > /dev/null 2>&1 || { echo "$0 requires ${req_comm[i]} but it's not available!" && exit 1; }
done

# Setup proper symlinks for Parrot Release
for element in "${DEBIAN_DIRS[@]}"
do
	sudo ln -sf ${element}/${DEBIAN_RELEASE} ${element}/${PARROT_DIST}
done

build-simple-cdd --verbose --debug --force-root --conf simple-cdd.conf --dist $PARROT_DIST
